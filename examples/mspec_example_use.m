

% typical usage: 
%   - run cell A to define the root directory
%   - load/process data by running one cell of type B (B1,B2,... just
%   correspond to different type of admissible syntaxes/processing
%   parameters
%   - load cell C to generate and save a report
%   (-optional: use command line as in cell D to do more stuff)

%% A: define root directory and go to it!
rootdir='~/git/mass_spec/examples/'; %CHANGE THIS % all filenames will be defined with respect to this, unless
%absolute path is explicitely given
cd(rootdir);

%% B1: simplest, just load one file, find all references
fnames='150714_53707_CA_INT_ModSearch.csv'; %CHANGE THIS
ms=mspec(fnames);


%% B2: load multiple files and pool, find all references, use absolute path

fnames={'/Users/cc/Dropbox/mass_spec/examples/160303-57519-MBP-Mis18_Modsearch_AspN.csv',...
    '/Users/cc/Dropbox/mass_spec/examples/160303-57519-MBP-Mis18_Modsearch_trypsin.csv'};
ms=mspec(fnames);

%% B3: load multiple files and pool, find all references
fnames={'160303-57519-MBP-Mis18_Modsearch_AspN.csv','160303-57519-MBP-Mis18_Modsearch_trypsin.csv'};
ms=mspec(fnames);


%% B4: load multiple files, find all references, keep processing separate between files (no pool);
fnames={'160303-57519-MBP-Mis18_Modsearch_AspN.csv','160303-57519-MBP-Mis18_Modsearch_trypsin.csv'}; %CHANGE THIS
fdesc={'AspN','trypsin'};%CHANGE THIS %descriptor for the anlysis, if empty default to {'1','2',...}
pooldata=0; %do no pool
ms=mspec(fnames,fdesc,[],pooldata);

%% B5: load data from one file, split references into domains (using a file to describe the domains)
fnames='150714_53707_CA_INT_ModSearch.csv';
fdesc='CA_INT';
domains_file='test_input.xlsx';
ms=mspec(fnames,fdesc,domains_file);


%% B6: load data from multiple file (pooled), split references into domains (using a file to describe the domains)
% here it will be the same as previous cell output because the 2 files are
% indetical and we remove duplicates!
fnames={'150714_53707_CA_INT_ModSearch.csv','150714_53707_CA_INT_ModSearch.csv'};
fdesc={'CA_INT','CA_INT2'};
domains_file='test_input.xlsx';
ms=mspec(fnames,fdesc,domains_file);

%% B7: load data from multiple file, do NOT pool, split references into domains (using a file to describe the domains)
% here it will be two copies of the same stuff but with different reference
% names
fnames={'150714_53707_CA_INT_ModSearch.csv','150714_53707_CA_INT_ModSearch.csv'};
fdesc={'CA_INT','CA_INT2'};
domains_file='test_input.xlsx';
pooldata=0;
ms=mspec(fnames,fdesc,domains_file,pooldata);

%% B8: load data from one file, split references into domains (describe the domains directly in the command)
fnames='150714_53707_CA_INT_ModSearch.csv';
fdesc='CA_INT';
domains={'H2B_Xenopus_Laevis',[1,50],'1to50';...
    'Human_CENP-A',-1,'GR';...
    'H2B_Xenopus_Laevis',[51,200],'51to200'};
ms=mspec(fnames,fdesc,domains);


%% C: generate report, and save
report_out='./output/report_example'; %CHANGE THIS %path is relative to rootdir, unless absolute path specified here
ms.generate_report(report_out);

%% D :Advanced use: we can play directly with the peptides by generating the object
msp=ms.create_peptides_dict();
msp(1).toTable