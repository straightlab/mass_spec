classdef mspec_peptides < handle
    properties
        data=table();
        ref='';
        ref_sequence='';
    end
    
    methods
        function obj=mspec_peptides()
        end
        
        function T=toTable(obj)
            T=table();
            for i=1:length(obj)
             T0=table(repmat({obj(i).ref},size(obj(i).data,1),1),...
                    'VariableNames',{'ref'});
                T=cat(1,T,cat(2,T0,obj(i).data));
            end    
        end
        
       
        function T=get_statistics(obj)
            %median length, mad length, score 
           ref={};
           length_median=[];
           length_mad=[];
           nu_cut=[];
           nu_score=[];
            for i=1:length(obj)
                lgth=obj(i).data{:,'stop'}-obj(i).data{:,'start'};
                length_median=cat(1,length_median,median(lgth));
                length_mad=cat(1,length_mad,1.3876*mad(lgth,1));
                nu_cut=cat(1,nu_cut,length(lgth));
                nu_score=cat(1,nu_score,sum(obj(i).data{:,'N'}));
                ref=cat(1,ref,{obj(i).ref});
                
                
            end
            T=table(ref,length_median,length_mad,nu_cut,nu_score,'VariableNames',{'ref','length_median','length_mad','Nu_cut','Nu_score'});                
        end
    end
end