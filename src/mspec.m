classdef mspec < handle
    
    properties
        data=table()
        ref='';
        dict=[];
        map=[];
        modifs=[];
        %peptides=mspec_peptide();
    end
    
    methods
        function obj=mspec(fnames,filedesc,refs,group_files)
            if nargin==0
            else
                if nargin<2 | isempty(filedesc);
                    if not(iscell(fnames))
                        filedesc={1};
                    else
                        filedesc=cellfun(@(x) num2str(x),num2cell((1:length(fnames))),'uni',0);
                    end
                end
                if nargin<3
                    refs=[];
                end
                if nargin<4
                    group_files=1;
                end
                
                if not(isempty(refs)) & not(iscell(refs))
                    if exist(refs,'file');
                        fprintf(1,'Reading references description from file: %s\n',refs);
                        ref0=refs;
                        refs=mspec.read_ref_source(ref0);
                    end
                end
                
                
                repdict={'StartPosition','start';...
                    'EndPosition','stop';...
                    'Reference','ref';...
                    'Max','score';...
                    'Peptide','peptide'};
                
                fprintf(1,'\nLOADING\n\n');
                if not(iscell(fnames))
                    
                    obj=mspec.create_mspec(fnames,repdict,refs,filedesc);
                else
                    if group_files
                        fprintf(1,'Data files will be pooled\n');
                        obj=mspec.create_mspec(fnames,repdict,refs,filedesc);
                    else
                        fprintf(1,'Data files will be processed separately\n');
                        fprintf(1,'[%g/%g] Processing file %s \n',1,length(fnames),fnames{1});
                        obj=mspec.create_mspec(fnames{1},repdict,refs,filedesc(1));
                        for j=1:length(obj)
                            obj(j).ref=[obj(j).ref '_' filedesc{1}];
                            obj(j).data.ref=cellfun(@(x) [x '_' filedesc{1}],...
                                obj(j).data{:,'ref'},'uni',0);
                        end
                        
                        for i=2:length(fnames)
                            fprintf(1,'[%g/%g] Processing file %s \n',i,length(fnames),fnames{i});
                            cms=mspec.create_mspec(fnames{i},repdict,refs,filedesc(i));
                            for j=1:length(cms)
                                cms(j).ref=[cms(j).ref '_' filedesc{i}];
                                cms(j).data.ref=cellfun(@(x) [x '_' filedesc{i}],...
                                    cms(j).data{:,'ref'},'uni',0);
                            end
                            obj=cat(1,obj,cms);
                        end
                    end
                end
                fprintf(1,'Produced the following references :\n')
                disp({obj(:).ref}');
                
                %                 [T,refs_k]=mspec.loaddata(fnames,repdict,refs,1,filedesc);
                %                 obj(length(refs_k),1)=mspec();
                %                 for i=1:length(refs_k);
                %                     cidxs=strcmp(T{:,'ref'},refs_k{i});
                %                     obj(i).data=T(cidxs,:);
                %                     obj(i).ref=refs_k{i};
                %
                %                     %obj.get_modif_counts();
                %                 end
                fprintf(1,'\nPARSING\n\n');
                obj.parse_data;
            end
        end
        
        
        
        function parse_data(obj)
            for i=1:length(obj)
                fprintf(1,'[%g/%g] Parsing data for reference : %s \n',i,length(obj),obj(i).ref);
                [d,map]=mspec.make_dict(obj(i).data.start,obj(i).data.stop,obj(i).data.peptide);
                obj(i).dict=d;
                obj(i).map=map;
                obj(i).modifs=mspec.get_modified_aa(d,[],0);
                
                
            end
        end
        
        function T=get_modif_counts(obj)
            for i=1:length(obj)
                T=obj(i).process(@(x) size(x,1),{'N'});
                fprintf('[%g/%g] Modification counts for reference : %s \n',i,length(obj),obj(i).ref);
                disp(T);
            end
        end
        
        function T=process(obj,fs,varnames,pairs)
            if nargin<4
                pairs=[];
            end
            if nargin<3
                varnames={};
            end
            if not(iscell(fs))
                fs={fs};
            end
            T=table();
            Tpeptides=table();
            for i=1:length(obj)
                cpairs=pairs;
                if isempty(cpairs)
                    cpairs=obj(i).modifs;
                end
                [stats,stats2,~]=mspec.get_stats2(obj(i).data,obj(i).dict,obj(i).map,fs,cpairs);
                T0=cat(1,mspec.stats2table(cpairs,stats,stats2,varnames));
                Tpeptide=table(repmat({obj(i).ref},size(T0,1),1),'VariableNames',{'ref'}); %
                Tpeptides=cat(1,Tpeptides,Tpeptide);
                
                T=cat(1,T,T0);
                %T.Properties.VariableNames
            end
            T=cat(2,Tpeptides,T);
            
            
        end
        
        function [T,s]=get_coverage(obj)
            T=table();
            for i=1:length(obj)
                cover=63*ones(size(obj(i).map,1),1);
                covered_idxs=obj(i).map(:,2)>0;
                cover(covered_idxs,1)=obj(i).dict(obj(i).map(covered_idxs,1),3);
                fs={@(x) mspec.f_count_unique_score(x),@(x) mspec.f_count_unique_cut(x)};
                pairs=cat(2,(1:length(cover))',cover,-1*ones(length(cover),1));
                coverunique=mspec.get_stats(obj(i).data,obj(i).dict,obj(i).map,fs,pairs);
                T0=table(repmat({obj(i).ref},size(cover,1),1),...
                    (1:length(cover))',char(cover),obj(i).map(:,2),coverunique(:,1),coverunique(:,2),...
                    'VariableNames',{'ref','num','aa','N','N_score','N_cut'});
                T=cat(1,T0,T);
                s{i,1}=(T.aa)';
            end
        end
        
        %function get_uncovered(obj)
        %end
        
        function idxs=find_peptides(obj,aanum,modif)
            if nargin<3
                modif=[];
            else
                if ischar(modif)
                    modif=int8(modif);
                end
            end
            for i=length(obj):-1:1
                idxs{i,1}=obj(i).dict(mspec.get_peptides(aanum,modif,obj(i).dict,obj(i).map),1);
            end
        end
        
        function [T,Tc,Traw,Tp,Tpstats]=generate_report(obj,rootdir,out_prfx)
            if nargin<3
                outfname=rootdir;
            else
                outfname=fullfile(rootdir,out_prfx);
            end
            
            f1=@(x) mspec.f_score_sum_unique(x);
            f2=@(x) size(x,1);
            fprintf(1,'Calculating stats...\n');
            T=obj.process({f2,f1},{'N','score'});
            fprintf(1,'Calculating coverage...\n');
            Tc=obj.get_coverage();
            fprintf(1,'Creating clean data table...\n');
            Traw=vertcat(obj(:).data);
            fprintf(1,'Calculating peptides representation...\n');
            msp=obj.create_peptides_dict();
            Tp=msp.toTable();
            Tpstats=msp.get_statistics();
            
            
            if nargin>1
                fprintf(1,'Writing stats to file %s \n',[outfname '_stats.csv']);
                writetable(T,[outfname '_stats.csv']);
                fprintf(1,'Writing coverage to file %s \n',[outfname '_dataClean.csv']);
                writetable(Tc,[outfname '_coverage.csv']);
                fprintf(1,'Writing clean table to file %s \n',[outfname '_stats.csv']);
                writetable(Traw,[outfname '_dataClean.csv']);
                fprintf(1,'Writing peptides representation to file %s \n',[outfname '_peptides.csv']);
                writetable(Tp,[outfname '_peptides.csv']);
                fprintf(1,'Writing peptides statistics to file %s \n',[outfname '_peptides_stats.csv']);
                writetable(Tpstats,[outfname '_peptides_stats.csv']);
            end
            fprintf(1,'DONE\n');
            
        end
        
        function [msp,ics]=create_peptides_dict(obj)
            no=length(obj);
            msp(no,1)=mspec_peptides();
            ics{no,1}=[];
            for i=1:no
                cdata=obj(i).data;
                [T,ia,ic]=unique(obj(i).data(:,{'start','stop'}),'rows');
                max_score=accumarray(ic,cdata{:,'score'},size(ia),@(x) max(x),nan);
                sum_score=accumarray(ic,cdata{:,'score'},size(ia),@(x) sum(unique(x)),nan);
                n_peptides=accumarray(ic,cdata{:,'score'},size(ia),@(x) numel(unique(x)),0);
                peptides_clean=cellfun(@(x) mspec.rmmodif(x),cdata{ia,{'peptide'}},'uni',0);
                T2=cat(2,T,table(peptides_clean,max_score,sum_score,n_peptides,...
                    'VariableNames',{'peptide','score_max','score_sum','N'}));
                msp(i).data=T2;
                msp(i).ref=obj(i).ref;
                ics{i,1}=ic;
            end
        end
    end
    
    methods (Static)
        
        function refnames=read_ref_source(fin)
            T=readtable(fin);
            n=size(T,1);
            idesc=find(strcmp(T.Properties.VariableNames,'desc'));
            
            if not(isempty(idesc))
                refdesc=T{:,idesc};
                if not(iscell(refdesc))
                    refdesc=cellfun(@(x) num2str(x),num2cell(refdesc),'uni',0);
                end
            else
                refdesc=cellfun(@(x) num2str(x),num2cell((1:n)'),'uni',0);
            end
            xs=[T{:,'start'},T{:,'stop'}];
            i0=find(xs(:,1)==-1 & xs(:,2)==-1);
            
            xs=mat2cell(xs,ones(size(xs,1),1),2);
            if not(isempty(i0));
                for j=1:length(i0)
                    xs{i0(j)}=-1;
                end
            end
            
            refnames=cat(2,T{:,'ref'},xs,refdesc);
            
            
            
        end
        function ms=create_mspec(fname,repdict,refnames,filedesc)
            
            
            
            %             issplit=0; %determine whether there is split or not based on the format of refnames
            %             if not(isempty(refnames))
            %             nn=size(refnames);
            %             if min(nn(1:2))>1
            %                 issplit=1;
            %                 fprintf('Data will be loaded in split mode\n');
            %             end
            %             end
            %
            %             if issplit
            %                 crefs=unique(refnames{:,1});
            %                 else
            %                 crefs=refnames;
            %             end
            %
            
            % first prepare the refs_range variable appropriately
            split=0;
            refs={};
            if not(isempty(refnames))
                if not(iscell(refnames))
                    refnames={refnames};
                end
                nn=size(refnames);
                if min(nn(1:2))==1 %
                    n0=length(refnames);
                    refs_range=cat(2,reshape(refnames,n0,1),num2cell(-1*ones(n0,1))); %,repmat('',{n
                    refs=refs_range(:,1);
                else
                    split=1;
                    fprintf('Data will be loaded in split mode\n');
                    refs_range=refnames;
                    refs=refnames(:,1);
                end
            end
            
            [T,refs_k,~]=mspec.loaddata(fname,repdict,refs,1,filedesc);
            
            if isempty(refs)
                n0=length(refs_k);
                refs_range=cat(2,reshape(refs_k,n0,1),num2cell(-1*ones(n0,1)));
            end
            
            [idxs,refs_out]=mspec.split_by_range(T,refs_range);
            actives=find(cellfun(@(x) not(isempty(x)), idxs));
            nact=length(actives);
            ms(nact,1)=mspec();
            
            
            
            for i=1:nact
                cidxs=idxs{actives(i)};
                
                cT=cat(2,T(cidxs,:),table(T{cidxs,'ref'},'VariableNames',{'ref0'}));
                crefout=refs_out{actives(i)};
                cT.ref=crefout;
                
                ms(i).ref=crefout{1};
                ms(i).data=cT;
                
                %obj.get_modif_counts();
            end
            
            
            
            
            
            
        end
        
        function [T,refs_k,refs_d]=loaddata(fname,repdict,refnames,verbose,filedesc)
            if nargin<3
                refnames=[];
            end
            if nargin<4
                verbose=1;
            end
            if nargin<5
                filedesc=[];
            end
            
            if not(iscell(fname))
                fname={fname};
                
            end
            
            
            nf=length(fname);
            %             refs_all={}
            refs_k={};
            refs_d={};
            
            T=table();
            
            unique_files=1;
            if  not(isempty(filedesc))
                if not(iscell(filedesc))
                    if (isscalar(filedesc) & length(filedesc)==1 & filedesc==0)
                        unique_files=0;
                    else
                        filedesc={filedesc};
                    end
                    
                else
                    
                    
                    
                end
            end
            
            
            for i=1:nf
                fprintf(1,'Reading data file %g out of %g: %s \n',i,nf, fname{i});
                
                [cT,crefs_k,crefs_d]=mspec.loaddata_single(fname{i},repdict,refnames,verbose);
                
                refs_k=union(crefs_k,refs_k);
                refs_d=union(crefs_d,refs_d);
                
                %split by
                if not(isempty(crefs_k))
                    
                    %[idxs,refs_out]=mspec.split_by_range(cT,
                    cT2=cT(ismember(cT{:,'ref'},crefs_k),:);
                    %xxx=size(cT2,1)
                    if unique_files
                        % filedesc
                        % i
                        if isempty(filedesc)
                            fileidT=table(i*ones(size(cT2,1),1),'VariableNames',{'file_id'});
                        else
                            %    xxi=filedesc(i)
                            %   xxn=size(cT2,1)
                            %   xx=repmat(filedesc(i),size(cT2,1),1)
                            fileidT=table(repmat(filedesc(i),size(cT2,1),1),'VariableNames',{'file_id'});
                        end
                        cT2=cat(2,fileidT,cT2);
                    else
                        %
                    end
                    T=cat(1,T,cT2);
                end
                
            end
            
            
            
        end
        
        function [T,refs_k,refs_d]=loaddata_single(fname,repdict,refnames,verbose)
            %auxiliary function called to lad a single file
            T=readtable(fname);
            
            %first fix variable names
            hnames=T.Properties.VariableNames;
            n=size(repdict,1);
            for i=1:n
                ix=find(strcmp(hnames,repdict{i,1}));
                if not(isempty(ix))
                    T.Properties.VariableNames(ix)=repdict(i,2);
                end
            end
            
            %then get rid of bad stuff, or stuff that we didn't ask for
            refs0=unique(T.ref);
            refs_all=refs0(cellfun(@(x) length(x)>1,refs0,'uni',1)); % get rid of rows with no explicit reference
            if isempty(refnames) % we need to manually find the reference
                
                
                i2keep=cellfun(@(x) not(isempty(x)| length(x)<2 | strcmp(x(1:2),'##')),refs_all,'uni',1);
                i2rm=cellfun(@(x) (isempty(x)| length(x)<2 | strcmp(x(1:2),'##')),refs_all,'uni',1);
                
                refs_k=refs_all(i2keep);
                refs_d=refs_all(i2rm);
                
                
            else % we only pick the ones we asked for
                refs_k=intersect(refnames,refs_all);
                refs_d=setxor(refnames,refs_all);
                
            end
            
            
            
            
            if verbose
                fprintf(1,'Found %g references \n',length(refs_all));
                display(cellstr(refs_all));
                
                fprintf(1,'Keeping %g references :\n',length(refs_k));
                display(cellstr(refs_k));
                fprintf(1,'Discarding %g references :\n',length(refs_d));
                display(cellstr(refs_d));
            end
        end
        
        
        function [idxs,refs_out]=split_by_range(T,refs) %if -1 do not split, leave desc empty
            
            
            
            refs_range=refs
            n=size(refs_range,1);
            %i2keep=false(n,1);
            %j=0;
            refs_out{n,1}={}; %=refs_range(:,1);
            idxs{n,1}=[];
            for i=1:n
                split=0;
                
                if length(refs_range{i,2})==1 & refs_range{i,2}==-1 % do not split
                    cidxs=strcmp(T{:,'ref'},refs_range{i,1});
                    
                else
                    split=1;
                    cidxs=strcmp(T{:,'ref'},refs_range{i,1}) & ...
                        ((T{:,'start'}>=refs_range{i,2}(1) & T{:,'start'}<=refs_range{i,2}(2)) | ...
                        (T{:,'stop'}>=refs_range{i,2}(1) & T{:,'stop'}<=refs_range{i,2}(2)));
                end
                idxs{i}=find(cidxs);
                cn=sum(cidxs);
                if split & cn>0
                    refs_out{i}=repmat({[refs_range{i,1} '_' refs_range{i,3}]},cn,1);
                else
                    refs_out{i}=repmat(refs_range(i,1),cn,1);
                end
                
            end
            
        end
        
        
        function [d,map]=make_dict(start,stop,peptide)
            naa=max(stop); %should be roughtly total number of aa
            npept=length(start);
            n=sum(stop-start+1); %total number of read aa n>>naa
            
            counts=zeros(naa,1);
            
            x=zeros(naa,1); %current count
            
            d=zeros(n,4);
            
            for i=1:npept
                rng=start(i):stop(i);
                counts(rng)=counts(rng)+1;
            end
            x1=cumsum([1;counts]);
            x1=x1(1:end-1);
            
            for i=1:npept
                rng=start(i):stop(i);
                pos=x1(rng)+x(rng);
                d(pos,1)=i;
                s=peptide{i};
                snum=(int8(s(3:end-2)))';
                %[ix,sy,aa,ix_u,aa_u]=mspec.get_modif(snum);
                [sy,aa]=mspec.get_modif_all(snum);
                %if not(isempty(ix))
                d(pos,2)=sy;
                d(pos,3)=aa;
                %end
                
                d(pos,4)=rng';
                x(rng)=x(rng)+1;
                
            end
            map=[x1,counts];
        end
        
        
        function [ix,sy,aa,sclean]=get_modif(snum)
            % s in uint8
            %snum=int8(s);
            ix=find(snum<65);
            
            sy=snum(ix);
            aa=[];
            if not(isempty(ix))
                aa=snum(ix-1);
                n=length(ix);
                ix=ix-(1:n)';
            end
            
            
        end
        
        function sclean=rmmodif(s)
            snum=int8(s(3:end-2));
            
            sclean=[s(1:2) char(snum(snum>64)) s(end-1:end)];
        end
        
        
        
        function [sy,aa]=get_modif_all(snum)
            % s in uint8
            %snum=int8(s);
            
            
            ix0=snum>64;
            aa=snum(ix0);
            sy=zeros(size(aa));
            ix=find(snum<65);
            if not(isempty(ix))
                sy_m=snum(ix);
                n=length(ix);
                ix=ix-(1:n)';
                sy(ix)=sy_m;
            end
            
        end
        
        
        function rng=get_range(i,map)
            rng=[map(i,1),(map(i,1)+map(i,2))-1];
            if rng(2)<rng(1)
                rng=[];
            end
        end
        
        function idxs=get_peptides(i,modif,d,map)
            %gives the indexes in the dictionnary
            
            idxs=[];
            rng=mspec.get_range(i,map);
            if not(isempty(rng))
                if not(isempty(modif)) & not(modif==-1) %[] or -1 accepts all modifs
                    idxs=find(ismember(d(rng(1):rng(2),2),modif));
                else
                    idxs=(1:(rng(2)-rng(1)+1))';
                end
                if not(isempty(idxs))
                    idxs=idxs+rng(1)-1;
                end
            end
        end
        
        function pairs=get_modified_aa(d,modifs,withunmodif)
            if nargin<3
                withunmodif=0;
            end
            
            if nargin<2 | isempty(modifs)
                
                i2keepM=not(d(:,2)==0);
                pairs=unique(d(i2keepM,[4,3,2]),'rows');
            else
                modif=[0,int8(modifs)];
                i2keepM=not(d(:,2)==0) & ismember(d(:,2),modif);
                pairs=unique(d(i2keepM,[4,3,2]),'rows');
            end
            if withunmodif
                u_aa=unique(pairs(:,1:2),'rows');
                pairs=cat(1,pairs,[u_aa,zeros(size(u_aa,1),1)]);
            end
        end
        
        function print_pairs(pairs)
            for i=1:size(pairs,1)
                fprintf(1,'%g: %s%g %s\n',i,char(pairs(i,2)),pairs(i,1),char(pairs(i,3)));
            end
        end
        
        function idx=get_pair_idx(pairs,aa,modif)
            idx=ind(pairs(:,1)==aa & pairs(:,3)==modif);
        end
        
        function [stats,pairs]=get_stats(T,d,map,fs,pairs)
            if nargin<5
                pairs=mspec.get_modified_aa(d);
            end
            
            nfs=length(fs);
            nmod=size(pairs,1);
            stats=nan(nmod,length(fs));
            
            for i=1:nmod
                idxs=mspec.get_peptides(pairs(i,1),pairs(i,3),d,map);
                %if not(isempty(idxs))
                Tidxs=d(idxs,1);
                for j=1:nfs
                    cf=fs{j};
                    
                    stats(i,j)=cf(T(Tidxs,:));
                    %end
                end
            end
        end
        
        
        
        function [stats,stats2,pairs]=get_stats2(T,d,map,fs,pairs)
            if nargin<5
                pairs=mspec.get_modified_aa(d,[],0);
            end
            u_aa=unique(pairs(:,1:2),'rows');
            pairs2=[u_aa,zeros(size(u_aa,1),1)];
            stats=mspec.get_stats(T,d,map,fs,pairs);
            stats2=mspec.get_stats(T,d,map,fs,pairs2);
        end
        
        function T=stats2table(pairs,stats,stats2,vnames)
            nstats=size(stats,2);
            vnames0=vnames;
            if nargin<4 | isempty(vnames);
                vnames=cellfun(@(x) sprintf('f%g',x),num2cell(1:nstats),'uni',0);
                
            end
            if nargin>2 & not(isempty(stats2))
                statsall=repmat(stats,1,3);
                statsall(:,1:3:end)=stats;
                statsall(:,2:3:end)=stats2;
                statsall(:,3:3:end)=stats./(stats+stats2);
                nstats=3*nstats;
                
                vnames2=cellfun(@(x) [x '_unmod'],vnames,'uni',0);
                vnames3=cellfun(@(x) [x '_ratio'],vnames,'uni',0);
                vnames0=cat(2,vnames,vnames,vnames);
                for k=1:length(vnames2)
                    
                    vnames0{3*k-1}=vnames2{k};
                    vnames0{3*k-2}=vnames{k};
                    vnames0{3*k}=vnames3{k};
                end
            else
                statsall=stats;
            end
            
            c=cat(2,{char(pairs(:,2)),pairs(:,1),char(pairs(:,3))},...
                mat2cell(statsall,size(statsall,1),ones(1,nstats)));
            vnames_all=cat(2,{'aa','num','modif'},vnames0);
            T=table(c{:},'VariableNames',vnames_all);
            
        end
        
        
        
        function y=f_score_sum_unique(T)
            if size(T,1)==0
                y=nan;
            else
                sc=T.score;
                [c,ia]=unique(sc);
                y=sum(c);
            end
        end
        
        %         function y=p_score_sum_unique(sc)
        %             if size(sc,1)==0
        %                 y=nan;
        %             else
        %             [c,ia]=unique(sc);
        %             y=sum(c);
        %             end
        %         end
        
        
        function y=f_count_unique_score(T)
            if size(T,1)==0
                y=nan;
            else
                sc=T.score;
                [c,ia]=unique(sc);
                y=length(c);
            end
        end
        
        
        
        function y=f_count_unique_cut(T)
            if size(T,1)==0
                y=nan;
            else
                
                [c,ia]=unique([T.start,T.stop],'rows');
                y=size(c,1);
            end
        end
        
        function y=f_count_unique_scoreAndCut(T)
            if size(T,1)==0
                y=nan;
            else
                
                [c,ia]=unique([T.start,T.stop,T.score],'rows');
                y=size(c,1);
            end
        end
        
        function Tc=compare_data(fnames,fdescs,varnames,fnameout,defassign)
            if nargin<5
                defassign=-1;
            end
            if nargin<4
                fnameout=[];
            end
            if nargin<3 | isempty(varnames)
                T=readtable(fnames{1});
                varnames=setxor(T.Properties.VariableNames,{'ref','aa','num','modif'});
            end
            if nargin<2 | isempty(fdescs)
                fdescs=cellfun(@(x) num2str(x),num2cell(1:length(fnames)),'uni',0);
            end
            
            allvarnames=cat(2,{'ref','aa','num','modif'},varnames);
            nvar=length(varnames);
            cT=mspec.read_table(fnames{1},allvarnames);
            T=cT(:,1:4);
            yall=table2array(cT(:,5:end));
            for i=2:length(fnames)
                cT=mspec.read_table(fnames{i},allvarnames);
                [~,ia,ib]=intersect(T,cT(:,1:4),'rows');
                cn=size(cT,1);
                ntot=size(T,1);
                %ncommon=length(ib);
                ib_unique=true(size(cn,1));
                ib_unique(ib)=false;
                
                y=defassign*ones(ntot,nvar);
                y(ia,:)=table2array(cT(ib,5:end));
                y=cat(1,y,table2array(cT(ib_unique,5:end)));
                yall=cat(1,yall,nan(sum(ib_unique),nvar*(i-1)));
                yall=cat(2,yall,y);
                
            end
            
            varnames2=repmat(varnames,1,length(fnames));
            nn=length(varnames2);
            %is=[];
            fdescs
            for k=1:length(fnames)
                varnames2(k:length(fnames):end)=...
                    cellfun(@(x) [x,'_',fdescs{k}],varnames,...
                    'uni',0);
                %   is=cat(2,is,k:length(fnames):nn);
            end
            nf=length(fnames);
            is=reshape(reshape(1:nn,nvar,nf)',1,nn);
            
            
            yall2=yall(:,is);
            
            Tc=cat(2,T,array2table(yall2,'VariableNames',varnames2));
            
            if not(isempty(fnameout))
                writetable(Tc,fnameout);
            end
        end
        
        
        function T=read_table(fname,varnames)
            cT=readtable(fname);
            [~,ix,~]=intersect(cT.Properties.VariableNames,varnames,'stable');
            if not(length(varnames)==length(ix))
                error('Missing variables');
            end
            T=cT(:,ix);
        end
        
        
    end
    
end

